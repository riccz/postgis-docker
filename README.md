# PostGIS Docker Image #

This is a Docker container image of a PostGIS 2.5 server on top of PostgreSQL
11. The three extensions `postgis`, `postgis_topolgy` and `postgis_sfcgal` are
included in the default template database (`template1`).

A database named after the environment variable `DBNAME` is automatically
created. A user named after `DBUSER` with password `DBPASS` is also
automatically created and given ownership of the database.
If you need such user to be a superuser, just set `DBUSER=postgres`.

## Known Issues and TODO ##

- This container relies on the automatic database initializtion done by the
  Debian packages. If the data or config volumes are cleared, the `main` cluster
  is not automatically recreated.
