#!/bin/sh
set -eu

chown -R "${PUID}:${PGID}" /etc/postgresql/
chown -R "${PUID}:${PGID}" /var/lib/postgresql/

gosu ${PUID}:${PGID} /usr/lib/postgresql/11/bin/postgres -D '/etc/postgresql/11/main/' -h '' &
POSTGRES_PID=$!
sleep 5
echo "\
SELECT 'CREATE DATABASE ${DBNAME}'
    WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = '${DBNAME}')
\gexec

SELECT 'CREATE USER ${DBUSER}'
    WHERE NOT EXISTS (SELECT FROM pg_roles WHERE rolname = '${DBUSER}')
\gexec

ALTER USER ${DBUSER} LOGIN PASSWORD '${DBPASS}';
ALTER DATABASE ${DBNAME} OWNER TO ${DBUSER};
" | gosu ${PUID}:${PGID} psql
kill "$POSTGRES_PID"
wait "$POSTGRES_PID"

exec gosu "${PUID}:${PGID}" "$@"
