FROM debian:buster-slim
LABEL maintainer="ricc@zanl.eu"

RUN export DEBIAN_FRONTEND=noninteractive && apt-get update && \
    apt-get install -y \
    gosu \
    postgresql-11 \
    postgresql-11-postgis-2.5

ENV PUID=postgres PGID=postgres

# Disable SSL
RUN sed -E 's/^\s*ssl\s*=\s*on\s*$/ssl = off/' -i /etc/postgresql/11/main/postgresql.conf

# Allow remote login with password
RUN echo '\nhost all all 0.0.0.0/0 md5\nhost all all ::/0 md5' >> \
    /etc/postgresql/11/main/pg_hba.conf

# Add PostGIS extensions to default DB template
RUN gosu ${PUID}:${PGID} /usr/lib/postgresql/11/bin/postgres -D '/etc/postgresql/11/main/' -h '' & \
    POSTGRES_PID=$! && \
    sleep 5 && \
    echo '\
    CREATE EXTENSION postgis; \
    CREATE EXTENSION postgis_topology; \
    CREATE EXTENSION postgis_sfcgal; \
    ' | gosu ${PUID}:${PGID} psql template1 && \
    kill "${POSTGRES_PID}" && \
    wait "${POSTGRES_PID}"

COPY docker-entrypoint.sh /usr/local/bin/

# WORKDIR /var/lib/postgresql/11/main

VOLUME /etc/postgresql
VOLUME /var/lib/postgresql

ENV DBNAME="mydb"
ENV DBUSER="mydb_user"
ENV DBPASS="mydb_pass"

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
CMD ["/usr/lib/postgresql/11/bin/postgres", \
    "-D", "/etc/postgresql/11/main/", \
    "-h", "*"]
